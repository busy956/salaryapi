package com.bj.salaryapi.controller;

import com.bj.salaryapi.model.SalaryItem;
import com.bj.salaryapi.model.SalaryPreTexChangeRequest;
import com.bj.salaryapi.model.SalaryRequest;
import com.bj.salaryapi.model.SalaryResponse;
import com.bj.salaryapi.service.SalaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/salary")
public class SalaryController {
    private final SalaryService salaryService;

    @PostMapping("/people")
    public String setSalary(@RequestBody SalaryRequest request) {
        salaryService.setSalary(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<SalaryItem> getSalarys() {
        return salaryService.getSalarys();
    }

    @GetMapping("/detail/{id}")
    public SalaryResponse getSalary(@PathVariable long id) {
        return salaryService.getSalary(id);
    }

    @PutMapping("/pre-tex/{id}")
    public String putSalary(@PathVariable long id, @RequestBody SalaryPreTexChangeRequest request) {
        salaryService.putSalary(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delSalary(@PathVariable long id) {
        salaryService.delSalary(id);

        return "OK";
    }
}
