package com.bj.salaryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.awt.image.renderable.RenderableImage;

@Getter
@AllArgsConstructor
public enum PositionStatus {
    SA_WON("사원"),
    DEA_RI("대리"),
    GUA_JANG("과장"),
    CHA_JANG("차장"),
    BU_JANG("부장");

    public final String positionStatus;

}
