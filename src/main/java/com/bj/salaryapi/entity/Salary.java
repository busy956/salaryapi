package com.bj.salaryapi.entity;

import com.bj.salaryapi.enums.PositionStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private PositionStatus position;

    @Column(nullable = false)
    private Double preTex;

    @Column(nullable = false)
    private Double pension;

    @Column(nullable = false)
    private Double health;

    @Column(nullable = false)
    private Double employ;

    @Column(nullable = false)
    private Double accident;

    @Column(nullable = false)
    private Double income;

    @Column(nullable = false)
    private Double texFree;

}
