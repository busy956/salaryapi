package com.bj.salaryapi.repository;

import com.bj.salaryapi.entity.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaryRepository extends JpaRepository<Salary, Long> {
}
