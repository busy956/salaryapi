package com.bj.salaryapi.service;

import com.bj.salaryapi.entity.Salary;
import com.bj.salaryapi.model.SalaryItem;
import com.bj.salaryapi.model.SalaryPreTexChangeRequest;
import com.bj.salaryapi.model.SalaryRequest;
import com.bj.salaryapi.model.SalaryResponse;
import com.bj.salaryapi.repository.SalaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SalaryService {
    private final SalaryRepository salaryRepository;

    public void setSalary(SalaryRequest request) {
        Salary addData = new Salary();
        addData.setName(request.getName());
        addData.setPosition(request.getPosition());
        addData.setPreTex(request.getPreTex());
        addData.setPension(request.getPension());
        addData.setHealth(request.getHealth());
        addData.setEmploy(request.getEmploy());
        addData.setAccident(request.getAccident());
        addData.setIncome(request.getIncome());
        addData.setTexFree(request.getTexFree());

        salaryRepository.save(addData);
    }

    public List<SalaryItem> getSalarys() {
        List<Salary> originData = salaryRepository.findAll();

        List<SalaryItem> result = new LinkedList<>();

        for (Salary salary : originData) {
            SalaryItem addItem = new SalaryItem();
            addItem.setId(salary.getId());
            addItem.setName(salary.getName());
            addItem.setPosition(salary.getPosition());
            addItem.setPreTex(salary.getPreTex());

            result.add(addItem);
        }
        return result;
    }

    public SalaryResponse getSalary(long id) {
        Salary originData = salaryRepository.findById(id).orElseThrow();

        SalaryResponse response = new SalaryResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPosition(originData.getPosition());
        response.setPreTex(originData.getPreTex());
        response.setPension(originData.getPension());
        response.setHealth(originData.getHealth());
        response.setEmploy(originData.getEmploy());
        response.setAccident(originData.getAccident());
        response.setIncome(originData.getIncome());
        response.setTexFree(originData.getTexFree());

        return response;
    }

    public void putSalary(long id, SalaryPreTexChangeRequest request) {
        Salary originData = salaryRepository.findById(id).orElseThrow();
        originData.setPreTex(request.getPreTex());

        salaryRepository.save(originData);
    }

    public void delSalary(long id) {
        salaryRepository.deleteById(id);
    }
}
