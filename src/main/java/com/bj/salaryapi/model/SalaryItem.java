package com.bj.salaryapi.model;

import com.bj.salaryapi.enums.PositionStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryItem {
    private Long id;
    private String name;
    private PositionStatus position;
    private Double preTex;
}
