package com.bj.salaryapi.model;

import com.bj.salaryapi.enums.PositionStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryResponse {
    private Long id;
    private String name;
    private PositionStatus position;
    private Double preTex;
    private Double pension;
    private Double health;
    private Double employ;
    private Double accident;
    private Double income;
    private Double texFree;
}
