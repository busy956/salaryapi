package com.bj.salaryapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryPreTexChangeRequest {
    private Double preTex;
}
